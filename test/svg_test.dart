import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_ambulance/resources/resources.dart';

void main() {
  test('svg assets test', () {
    expect(File(Svg.ambulanceSvg).existsSync(), isTrue);
    expect(File(Svg.bookmarkSvg).existsSync(), isTrue);
    expect(File(Svg.spreadsheetSvg).existsSync(), isTrue);
    expect(File(Svg.userPlusSvg).existsSync(), isTrue);
    expect(File(Svg.userSvg).existsSync(), isTrue);
  });
}
