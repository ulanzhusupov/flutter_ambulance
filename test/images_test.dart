import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_ambulance/resources/resources.dart';

void main() {
  test('images assets test', () {
    expect(File(Images.articleAvatar).existsSync(), isTrue);
    expect(File(Images.articleCover).existsSync(), isTrue);
    expect(File(Images.authorPhoto).existsSync(), isTrue);
    expect(File(Images.avatar1).existsSync(), isTrue);
    expect(File(Images.bell).existsSync(), isTrue);
    expect(File(Images.bxSpreadsheetSvg).existsSync(), isTrue);
    expect(File(Images.clipboard).existsSync(), isTrue);
    expect(File(Images.clipboardMax).existsSync(), isTrue);
    expect(File(Images.fileFolder).existsSync(), isTrue);
    expect(File(Images.hospital).existsSync(), isTrue);
    expect(File(Images.pageWithCurl).existsSync(), isTrue);
    expect(File(Images.sleepCover).existsSync(), isTrue);
    expect(File(Images.speech).existsSync(), isTrue);
    expect(File(Images.upAndDown).existsSync(), isTrue);
    expect(File(Images.userPlus).existsSync(), isTrue);
  });
}
