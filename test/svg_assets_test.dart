import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_ambulance/resources/resources.dart';

void main() {
  test('svg_assets assets test', () {
    expect(File(SvgAssets.addDoc).existsSync(), isTrue);
    expect(File(SvgAssets.ambulanceSvg).existsSync(), isTrue);
    expect(File(SvgAssets.bell).existsSync(), isTrue);
    expect(File(SvgAssets.bookmarkSvg).existsSync(), isTrue);
    expect(File(SvgAssets.collectionSvg).existsSync(), isTrue);
    expect(File(SvgAssets.doctorCommentsSvg).existsSync(), isTrue);
    expect(File(SvgAssets.eyedropperSvg).existsSync(), isTrue);
    expect(File(SvgAssets.locationSvg).existsSync(), isTrue);
    expect(File(SvgAssets.messageDoctorSvg).existsSync(), isTrue);
    expect(File(SvgAssets.recomendSvg).existsSync(), isTrue);
    expect(File(SvgAssets.spreadsheetSvg).existsSync(), isTrue);
    expect(File(SvgAssets.star).existsSync(), isTrue);
    expect(File(SvgAssets.userPinSvg).existsSync(), isTrue);
    expect(File(SvgAssets.userPlusSvg).existsSync(), isTrue);
    expect(File(SvgAssets.userSvg).existsSync(), isTrue);
  });
}
