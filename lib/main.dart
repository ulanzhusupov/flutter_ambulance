import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ambulance/presentation/screens/splash_screen.dart';
import 'package:flutter_ambulance/presentation/theme/app_colors.dart';
import 'package:flutter_ambulance/presentation/widgets/shared_pref_widget.dart';
import 'package:flutter_ambulance/presentation/widgets/textfield_unfocus.dart';
import 'package:flutter_ambulance/routes/app_router.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(375, 812),
      builder: (context, child) {
        return TextFieldUnfocus(
          child: SharedPrefWidget(
            child: MaterialApp.router(
              routerConfig: AppRouter().config(),
              title: 'Flutter Demo',
              debugShowCheckedModeBanner: false,
              theme: ThemeData(
                  colorScheme:
                      ColorScheme.fromSeed(seedColor: AppColors.buttonColor),
                  useMaterial3: true,
                  fontFamily: "sf pro"),
              // home: SharedPrefWidget(child: const SplashScreen()),
            ),
          ),
        );
      },
    );
  }
}
