import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ambulance/data/models/article_model.dart';
import 'package:flutter_ambulance/data/models/doctors_model.dart';
import 'package:flutter_ambulance/presentation/screens/home/article_page_screen.dart';
import 'package:flutter_ambulance/presentation/screens/home/articles_screen.dart';
import 'package:flutter_ambulance/presentation/screens/home/doctor_info_screen.dart';
import 'package:flutter_ambulance/presentation/screens/home/doctors_screen.dart';
import 'package:flutter_ambulance/presentation/screens/home/home_screen.dart';
import 'package:flutter_ambulance/presentation/screens/home/profile_screen.dart';
import 'package:flutter_ambulance/presentation/screens/splash_screen.dart';

part 'app_router.gr.dart';

@AutoRouterConfig()
class AppRouter extends _$AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(
          path: "/",
          page: HomeRoute.page,
          initial: true,
        ),
        AutoRoute(
          page: DoctorsRoute.page,
        ),
        AutoRoute(page: DoctorInfoRoute.page),
        AutoRoute(page: ProfileRoute.page),
        AutoRoute(page: ArticlesRoute.page),
        AutoRoute(page: ArticleRouteRoute.page),
        AutoRoute(page: SplashRoute.page),
      ];
}
