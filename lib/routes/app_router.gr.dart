// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

part of 'app_router.dart';

abstract class _$AppRouter extends RootStackRouter {
  // ignore: unused_element
  _$AppRouter({super.navigatorKey});

  @override
  final Map<String, PageFactory> pagesMap = {
    ArticleRouteRoute.name: (routeData) {
      final args = routeData.argsAs<ArticleRouteRouteArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: ArticlePageScreen(
          key: args.key,
          model: args.model,
        ),
      );
    },
    ArticlesRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const ArticlesScreen(),
      );
    },
    DoctorInfoRoute.name: (routeData) {
      final args = routeData.argsAs<DoctorInfoRouteArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: DoctorInfoScreen(
          key: args.key,
          model: args.model,
        ),
      );
    },
    DoctorsRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const DoctorsScreen(),
      );
    },
    HomeRoute.name: (routeData) {
      final args =
          routeData.argsAs<HomeRouteArgs>(orElse: () => const HomeRouteArgs());
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: HomeScreen(key: args.key),
      );
    },
    ProfileRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const ProfileScreen(),
      );
    },
    SplashRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const SplashScreen(),
      );
    },
  };
}

/// generated route for
/// [ArticlePageScreen]
class ArticleRouteRoute extends PageRouteInfo<ArticleRouteRouteArgs> {
  ArticleRouteRoute({
    Key? key,
    required ArticleModel model,
    List<PageRouteInfo>? children,
  }) : super(
          ArticleRouteRoute.name,
          args: ArticleRouteRouteArgs(
            key: key,
            model: model,
          ),
          initialChildren: children,
        );

  static const String name = 'ArticleRouteRoute';

  static const PageInfo<ArticleRouteRouteArgs> page =
      PageInfo<ArticleRouteRouteArgs>(name);
}

class ArticleRouteRouteArgs {
  const ArticleRouteRouteArgs({
    this.key,
    required this.model,
  });

  final Key? key;

  final ArticleModel model;

  @override
  String toString() {
    return 'ArticleRouteRouteArgs{key: $key, model: $model}';
  }
}

/// generated route for
/// [ArticlesScreen]
class ArticlesRoute extends PageRouteInfo<void> {
  const ArticlesRoute({List<PageRouteInfo>? children})
      : super(
          ArticlesRoute.name,
          initialChildren: children,
        );

  static const String name = 'ArticlesRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [DoctorInfoScreen]
class DoctorInfoRoute extends PageRouteInfo<DoctorInfoRouteArgs> {
  DoctorInfoRoute({
    Key? key,
    required DoctorsModel model,
    List<PageRouteInfo>? children,
  }) : super(
          DoctorInfoRoute.name,
          args: DoctorInfoRouteArgs(
            key: key,
            model: model,
          ),
          initialChildren: children,
        );

  static const String name = 'DoctorInfoRoute';

  static const PageInfo<DoctorInfoRouteArgs> page =
      PageInfo<DoctorInfoRouteArgs>(name);
}

class DoctorInfoRouteArgs {
  const DoctorInfoRouteArgs({
    this.key,
    required this.model,
  });

  final Key? key;

  final DoctorsModel model;

  @override
  String toString() {
    return 'DoctorInfoRouteArgs{key: $key, model: $model}';
  }
}

/// generated route for
/// [DoctorsScreen]
class DoctorsRoute extends PageRouteInfo<void> {
  const DoctorsRoute({List<PageRouteInfo>? children})
      : super(
          DoctorsRoute.name,
          initialChildren: children,
        );

  static const String name = 'DoctorsRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [HomeScreen]
class HomeRoute extends PageRouteInfo<HomeRouteArgs> {
  HomeRoute({
    Key? key,
    List<PageRouteInfo>? children,
  }) : super(
          HomeRoute.name,
          args: HomeRouteArgs(key: key),
          initialChildren: children,
        );

  static const String name = 'HomeRoute';

  static const PageInfo<HomeRouteArgs> page = PageInfo<HomeRouteArgs>(name);
}

class HomeRouteArgs {
  const HomeRouteArgs({this.key});

  final Key? key;

  @override
  String toString() {
    return 'HomeRouteArgs{key: $key}';
  }
}

/// generated route for
/// [ProfileScreen]
class ProfileRoute extends PageRouteInfo<void> {
  const ProfileRoute({List<PageRouteInfo>? children})
      : super(
          ProfileRoute.name,
          initialChildren: children,
        );

  static const String name = 'ProfileRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [SplashScreen]
class SplashRoute extends PageRouteInfo<void> {
  const SplashRoute({List<PageRouteInfo>? children})
      : super(
          SplashRoute.name,
          initialChildren: children,
        );

  static const String name = 'SplashRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}
