import 'package:flutter/material.dart';
import 'package:flutter_ambulance/presentation/theme/app_colors.dart';
import 'package:flutter_ambulance/presentation/theme/app_fonts.dart';

class CodeTextField extends StatelessWidget {
  const CodeTextField(
      {super.key,
      required this.codeController,
      this.errorText,
      required this.hintText,
      required this.onChanged});

  final TextEditingController codeController;
  final String? errorText;
  final String hintText;
  final Function(String) onChanged;

  @override
  Widget build(BuildContext context) {
    return TextField(
      onChanged: onChanged,
      controller: codeController,
      maxLength: 4,
      textAlign: TextAlign.center,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        errorText: errorText,
        prefix: const Text(
          "Код",
          style: AppFonts.s18W600,
        ),
        suffix: IconButton(
          onPressed: () {
            codeController.clear();
          },
          icon: const Icon(Icons.close_rounded),
        ),
        hintText: hintText,
        hintStyle: AppFonts.s18W600.copyWith(color: AppColors.grey),
        counterText: "",
        enabledBorder: const UnderlineInputBorder(
          borderSide: BorderSide(width: 1, color: AppColors.grey),
        ),
      ),
    );
  }
}
