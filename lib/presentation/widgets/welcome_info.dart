import 'package:flutter/material.dart';
import 'package:flutter_ambulance/presentation/theme/app_fonts.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class WelcomeInfoItem extends StatelessWidget {
  const WelcomeInfoItem({super.key, required this.image, required this.text});

  final String image;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15),
      child: Row(
        children: [
          Image.asset(
            image,
            width: 32,
          ),
          SizedBox(width: 18.w),
          Flexible(
            child: Text(
              text,
              style: AppFonts.s15W400,
            ),
          ),
        ],
      ),
    );
  }
}
