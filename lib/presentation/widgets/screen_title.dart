import 'package:flutter/material.dart';
import 'package:flutter_ambulance/presentation/theme/app_fonts.dart';

class ScreenTitle extends StatelessWidget {
  const ScreenTitle({
    super.key,
    required this.title,
  });

  final String title;

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: AppFonts.s17W600,
    );
  }
}
