import 'package:flutter/material.dart';
import 'package:flutter_ambulance/presentation/theme/app_colors.dart';
import 'package:flutter_ambulance/presentation/theme/app_fonts.dart';

class MyChip extends StatelessWidget {
  const MyChip({
    super.key,
    required this.title,
    required this.isSelected,
    required this.onSelected,
  });
  final bool isSelected;
  final String title;
  final Function(bool) onSelected;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 5),
      child: ChoiceChip(
        onSelected: onSelected,
        showCheckmark: false,
        selectedColor: AppColors.buttonColor,
        backgroundColor: AppColors.white,
        label: Text(
          title,
          style: AppFonts.s15W500.copyWith(
              color: isSelected ? AppColors.white : AppColors.greyText),
        ),
        avatarBorder: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(4),
          side: const BorderSide(color: AppColors.greyText),
        ),
        selected: isSelected,
      ),
    );
  }
}
