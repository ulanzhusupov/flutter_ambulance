import 'package:flutter/material.dart';
import 'package:flutter_ambulance/presentation/theme/app_colors.dart';
import 'package:flutter_ambulance/presentation/theme/app_fonts.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AppButton extends StatelessWidget {
  const AppButton({
    super.key,
    required this.onPressed,
    required this.name,
  });

  final Function()? onPressed;
  final String name;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        width: 296.w,
        height: 54.h,
        child: ElevatedButton(
          onPressed: onPressed,
          style: ElevatedButton.styleFrom(
              backgroundColor: AppColors.buttonColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4))),
          child: Text(
            name,
            style: AppFonts.s18W600.copyWith(color: AppColors.white),
          ),
        ),
      ),
    );
  }
}
