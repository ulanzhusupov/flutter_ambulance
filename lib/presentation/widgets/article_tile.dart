import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ambulance/data/models/article_model.dart';
import 'package:flutter_ambulance/presentation/screens/home/article_page_screen.dart';
import 'package:flutter_ambulance/presentation/theme/app_colors.dart';
import 'package:flutter_ambulance/presentation/theme/app_fonts.dart';
import 'package:flutter_ambulance/routes/app_router.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ArticleTile extends StatelessWidget {
  const ArticleTile({
    super.key,
    required this.articleModel,
  });

  final ArticleModel articleModel;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: InkWell(
        onTap: () {
          context.router.push(ArticleRouteRoute(model: articleModel));
          // Navigator.push(
          //   context,
          //   MaterialPageRoute(
          //     builder: (context) => ArticlePageScreen(
          //       model: articleModel,
          //     ),
          //   ),
          // );
        },
        child: Container(
          width: 335.w,
          height: 93.h,
          decoration: BoxDecoration(
            color: AppColors.white,
            borderRadius: BorderRadius.circular(4.r),
          ),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                SizedBox(
                  width: 249.w,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        articleModel.title,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: AppFonts.s17W500,
                      ),
                      const Spacer(),
                      Text(
                        articleModel.date,
                        style: AppFonts.s15W300
                            .copyWith(color: AppColors.greyText),
                      )
                    ],
                  ),
                ),
                SizedBox(width: 10.w),
                ClipRRect(
                  borderRadius: BorderRadius.circular(2.0),
                  child: Image.asset(
                    articleModel.cover,
                    width: 56.w,
                    height: 56.w,
                    fit: BoxFit.cover,
                    scale: 5,
                    alignment: Alignment.center,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
