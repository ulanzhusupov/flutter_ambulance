import 'package:flutter/material.dart';
import 'package:flutter_ambulance/presentation/theme/app_colors.dart';
import 'package:flutter_ambulance/presentation/theme/app_fonts.dart';
import 'package:flutter_ambulance/presentation/widgets/app_button.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DoctorContacts extends StatelessWidget {
  const DoctorContacts(
      {super.key,
      required this.phoneNumber,
      required this.address,
      required this.email});
  final String phoneNumber;
  final String address;
  final String email;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.w),
        child: ListView(
          physics: const ScrollPhysics(),
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: Row(
                children: [
                  Icon(
                    Icons.phone,
                    size: 24.w,
                    color: AppColors.greyText,
                  ),
                  SizedBox(width: 19.w),
                  Text(
                    phoneNumber,
                    style: AppFonts.s18W400,
                  ),
                ],
              ),
            ),
            Divider(height: 0.5, color: AppColors.doctorDividerColor),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: Row(
                children: [
                  Icon(
                    Icons.location_city,
                    size: 24.w,
                    color: AppColors.greyText,
                  ),
                  SizedBox(width: 19.w),
                  Flexible(
                    child: Text(
                      address,
                      style: AppFonts.s18W400,
                    ),
                  ),
                ],
              ),
            ),
            Divider(height: 0.5, color: AppColors.doctorDividerColor),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: Row(
                children: [
                  Icon(
                    Icons.mail,
                    size: 24.w,
                    color: AppColors.greyText,
                  ),
                  SizedBox(width: 19.w),
                  Flexible(
                    child: Text(
                      email,
                      style: AppFonts.s18W400,
                    ),
                  ),
                ],
              ),
            ),
            Divider(height: 0.5, color: AppColors.doctorDividerColor),
            SizedBox(height: 15.h),
            AppButton(onPressed: () {}, name: "Записаться"),
          ],
        ),
      ),
    );
  }
}
