import 'package:flutter/material.dart';
import 'package:flutter_ambulance/presentation/theme/app_colors.dart';
import 'package:flutter_ambulance/presentation/theme/app_fonts.dart';
import 'package:flutter_ambulance/presentation/widgets/app_button.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DoctorsFeedback extends StatelessWidget {
  const DoctorsFeedback(
      {super.key, required this.itemCount, required this.model});

  final int itemCount;
  final List model;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: [
          Expanded(
            child: ListView.builder(
              physics: const ScrollPhysics(parent: BouncingScrollPhysics()),
              itemCount: itemCount,
              itemBuilder: (context, index) => ListTile(
                titleAlignment: ListTileTitleAlignment.top,
                leading: Image.asset(model[index].photo, width: 40.w),
                title: Text(
                  model[index].name,
                  style: AppFonts.s18W600,
                ),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      model[index].text,
                      style: AppFonts.s15W400,
                    ),
                    SizedBox(height: 8.h),
                    Text(
                      model[index].date,
                      style: AppFonts.s13W400.copyWith(
                        color: AppColors.greyText,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 8.h),
          AppButton(onPressed: () {}, name: "Записаться"),
        ],
      ),
    );
  }
}
