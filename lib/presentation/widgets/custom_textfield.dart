import 'package:flutter/material.dart';
import 'package:flutter_ambulance/presentation/theme/app_colors.dart';
import 'package:flutter_ambulance/presentation/theme/app_fonts.dart';

class CustomTextField extends StatelessWidget {
  const CustomTextField({
    super.key,
    required this.hintText,
    required this.prefixText,
    required this.controller,
    this.textAlign = TextAlign.start,
    this.maxLength,
    this.onChanged,
  });
  final String hintText;
  final String prefixText;
  final TextAlign textAlign;
  final TextEditingController controller;
  final int? maxLength;
  final Function(String)? onChanged;

  @override
  Widget build(BuildContext context) {
    return TextField(
      onChanged: onChanged,
      controller: controller,
      maxLength: maxLength,
      textAlign: textAlign,
      keyboardType: TextInputType.number,
      style: AppFonts.s18W600,
      decoration: InputDecoration(
        prefixIcon: Padding(
          padding: EdgeInsets.only(top: 10.5),
          child: Text(
            prefixText,
            style: AppFonts.s18W600,
          ),
        ),
        prefixIconConstraints: const BoxConstraints(
          minWidth: 0,
          minHeight: 0,
          maxWidth: 15,
        ),
        suffix: IconButton(
          onPressed: () {
            controller.clear();
          },
          icon: const Icon(Icons.close_rounded),
        ),
        hintText: hintText,
        hintStyle: AppFonts.s18W600.copyWith(color: AppColors.grey),
        counterText: "",
        enabledBorder: const UnderlineInputBorder(
          borderSide: BorderSide(width: 2, color: AppColors.borderColor),
        ),
      ),
    );
  }
}
