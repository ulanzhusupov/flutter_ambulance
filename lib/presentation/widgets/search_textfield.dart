import 'package:flutter/material.dart';
import 'package:flutter_ambulance/presentation/theme/app_colors.dart';
import 'package:flutter_ambulance/presentation/theme/app_fonts.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SearchTextField extends StatelessWidget {
  const SearchTextField({
    super.key,
    required this.controller,
    required this.hintText,
    this.onChanged,
  });
  final TextEditingController controller;
  final String hintText;
  final Function(String)? onChanged;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 36.h,
      width: MediaQuery.of(context).size.width * 0.7,
      child: TextField(
        controller: controller,
        onChanged: onChanged,
        decoration: InputDecoration(
          prefixIcon: Icon(
            Icons.search,
            color: AppColors.searchHintColor,
          ),
          hintText: hintText,
          contentPadding: const EdgeInsets.all(0),
          hintStyle:
              AppFonts.s17W400.copyWith(color: AppColors.searchHintColor),
          fillColor: AppColors.searchColorOpacity,
          filled: true,
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: AppColors.searchColorOpacity),
            borderRadius: BorderRadius.circular(10),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: AppColors.searchColorOpacity),
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }
}
