import 'package:flutter/material.dart';
import 'package:flutter_ambulance/presentation/theme/app_colors.dart';
import 'package:flutter_ambulance/presentation/theme/app_fonts.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';

class CustomTabbar extends StatelessWidget {
  const CustomTabbar(
      {super.key,
      required this.tab1,
      required this.tab2,
      required this.tab3,
      required this.tab1IconStr,
      required this.tab2IconStr,
      required this.tab3IconStr,
      this.controller});

  final TabController? controller;

  final String tab1;
  final String tab2;
  final String tab3;

  final String tab1IconStr;
  final String tab2IconStr;
  final String tab3IconStr;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: TabBar(
        controller: controller,
        labelStyle: AppFonts.s15W500,
        indicatorSize: TabBarIndicatorSize.tab,
        indicatorColor: AppColors.buttonColor,
        unselectedLabelColor: AppColors.lightBlue,
        labelPadding: const EdgeInsets.only(bottom: 5),
        tabs: [
          Tab(
            icon: SvgPicture.asset(
              tab1IconStr,
              width: 28.h,
            ),
            text: tab1,
          ),
          Tab(
            icon: SvgPicture.asset(
              tab2IconStr,
              width: 28.h,
            ),
            text: tab2,
          ),
          Tab(
            icon: SvgPicture.asset(
              tab3IconStr,
              width: 28.h,
            ),
            text: tab3,
          ),
        ],
      ),
    );
  }
}
