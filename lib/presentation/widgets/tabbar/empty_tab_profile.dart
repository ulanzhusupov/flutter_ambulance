import 'package:flutter/material.dart';
import 'package:flutter_ambulance/presentation/theme/app_colors.dart';
import 'package:flutter_ambulance/presentation/theme/app_fonts.dart';
import 'package:flutter_ambulance/resources/resources.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';

class EmptyTabProfile extends StatelessWidget {
  const EmptyTabProfile({
    super.key,
    required this.text,
    required this.iconPath,
    this.addDocument,
  });

  final String text;
  final String iconPath;
  final Function()? addDocument;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 31),
        child: Column(
          children: [
            SizedBox(height: 32.h),
            Image.asset(
              iconPath,
              height: 105.h,
            ),
            SizedBox(height: 22.h),
            Text(
              text,
              style: AppFonts.s15W500.copyWith(color: AppColors.greyText),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 31.h),
            addDocument != null
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        SvgAssets.addDoc,
                        width: 18.w,
                        height: 20.h,
                      ),
                      SizedBox(width: 8.w),
                      InkWell(
                        onTap: addDocument,
                        child: Text(
                          "Добавить документ",
                          style: AppFonts.s15W500
                              .copyWith(color: AppColors.buttonColor),
                        ),
                      ),
                    ],
                  )
                : const SizedBox(),
          ],
        ),
      ),
    );
  }
}
