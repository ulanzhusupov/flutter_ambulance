import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ambulance/presentation/widgets/tabbar/empty_tab_profile.dart';
import 'package:flutter_ambulance/resources/resources.dart';

class ProfileTabbarView extends StatefulWidget {
  const ProfileTabbarView({
    super.key,
  });

  @override
  State<ProfileTabbarView> createState() => _ProfileTabbarViewState();
}

class _ProfileTabbarViewState extends State<ProfileTabbarView> {
  File? pickedFile;

  Future<void> _openFilePicker() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles();

    if (result != null) {
      pickedFile = File(result.files.single.path!);
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return TabBarView(
      children: [
        EmptyTabProfile(
          text: pickedFile != null
              ? pickedFile.toString()
              : "У вас пока нет добавленных результатов анализов",
          iconPath: Images.clipboard,
          addDocument: _openFilePicker,
        ),
        EmptyTabProfile(
          text: pickedFile != null
              ? pickedFile.toString()
              : "У вас пока нет добавленных диагнозов",
          iconPath: Images.fileFolder,
          addDocument: _openFilePicker,
        ),
        const EmptyTabProfile(
          text: "У вас пока нет рекомендаций",
          iconPath: Images.pageWithCurl,
        ),
      ],
    );
  }
}
