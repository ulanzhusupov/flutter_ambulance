import 'package:flutter/material.dart';
import 'package:flutter_ambulance/data/models/doctors_model.dart';
import 'package:flutter_ambulance/presentation/theme/app_colors.dart';
import 'package:flutter_ambulance/presentation/theme/app_fonts.dart';
import 'package:flutter_ambulance/resources/resources.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';

class DoctorsTile extends StatelessWidget {
  const DoctorsTile({super.key, required this.model, required this.onTap});
  final Function() onTap;
  final DoctorsModel model;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4),
      child: Column(
        children: [
          InkWell(
            onTap: onTap,
            child: Container(
              width: 336.w,
              height: 76.h,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                color: AppColors.white,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CircleAvatar(
                    backgroundImage: NetworkImage(model.image),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        model.speciality,
                        style: AppFonts.s15W300,
                      ),
                      Text(
                        model.name,
                        style: AppFonts.s17W500,
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        SvgAssets.star,
                        width: 22.w,
                      ),
                      Text(
                        model.rating.toString(),
                        style: AppFonts.s15W400.copyWith(
                          color: AppColors.greyText,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        SvgAssets.doctorCommentsSvg,
                        width: 23.w,
                      ),
                      Text(
                        model.feedback.length.toString(),
                        style: AppFonts.s15W400.copyWith(
                          color: AppColors.greyText,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
