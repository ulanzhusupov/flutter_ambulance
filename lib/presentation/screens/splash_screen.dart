import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ambulance/core/app_consts.dart';
import 'package:flutter_ambulance/presentation/screens/auth/welcome_screen.dart';
import 'package:flutter_ambulance/presentation/screens/home/home_screen.dart';
import 'package:flutter_ambulance/presentation/widgets/shared_pref_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

@RoutePage()
class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    routing();
  }

  void routing() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isLogined = prefs.getBool(AppConsts.isLogined) ?? false;
    await Future.delayed(const Duration(seconds: 1));
    if (isLogined) {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomeScreen()));
    } else {
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => const WelcomeScreen()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
