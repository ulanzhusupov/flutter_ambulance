import 'package:flutter/material.dart';
import 'package:flutter_ambulance/core/app_consts.dart';
import 'package:flutter_ambulance/presentation/screens/home/home_screen.dart';
import 'package:flutter_ambulance/presentation/theme/app_colors.dart';
import 'package:flutter_ambulance/presentation/theme/app_fonts.dart';
import 'package:flutter_ambulance/presentation/widgets/app_button.dart';
import 'package:flutter_ambulance/presentation/widgets/screen_title.dart';
import 'package:flutter_ambulance/presentation/widgets/shared_pref_widget.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CreateProfile extends StatelessWidget {
  const CreateProfile({super.key});

  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = TextEditingController();
    TextEditingController surnameController = TextEditingController();

    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: const ScreenTitle(title: "Создание профиля"),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.close),
        ),
        centerTitle: true,
        elevation: 1,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 31),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 34.h),
            const Text("Имя", style: AppFonts.s15W400),
            TextField(
              controller: nameController,
              textCapitalization: TextCapitalization.sentences,
              decoration: InputDecoration(
                hintText: "Введите ваше имя",
                hintStyle:
                    AppFonts.s17W400.copyWith(color: AppColors.lightGrey),
                enabledBorder: const UnderlineInputBorder(
                    borderSide: BorderSide(color: AppColors.black, width: 1)),
              ),
            ),
            SizedBox(height: 32.h),
            const Text("Фамилия", style: AppFonts.s15W400),
            TextField(
              textCapitalization: TextCapitalization.sentences,
              controller: surnameController,
              decoration: InputDecoration(
                hintText: "Введите вашу фамилию",
                hintStyle:
                    AppFonts.s17W400.copyWith(color: AppColors.lightGrey),
                enabledBorder: const UnderlineInputBorder(
                  borderSide: BorderSide(color: AppColors.black, width: 1),
                ),
              ),
            ),
            SizedBox(height: 50.h),
            const Spacer(),
            AppButton(
                onPressed: () async {
                  final SharedPreferences prefs = SharedPrefWidget.prefs;
                  await prefs.setString(AppConsts.name, nameController.text);
                  await prefs.setString(
                      AppConsts.surname, surnameController.text);
                  await prefs.setBool(AppConsts.isLogined, true);
                  // ignore: use_build_context_synchronously
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => HomeScreen()));
                },
                name: "Далее"),
            SizedBox(height: 25.h),
          ],
        ),
      ),
    );
  }
}
