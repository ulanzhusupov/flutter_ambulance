import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_ambulance/presentation/screens/auth/create_profile.dart';
import 'package:flutter_ambulance/presentation/theme/app_fonts.dart';
import 'package:flutter_ambulance/presentation/widgets/app_button.dart';
import 'package:flutter_ambulance/presentation/widgets/code_textfield.dart';
import 'package:flutter_ambulance/presentation/widgets/screen_title.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

// ignore: must_be_immutable
class ConfirmScreen extends StatefulWidget {
  ConfirmScreen({super.key, required this.code});

  int code;

  @override
  State<ConfirmScreen> createState() => _ConfirmScreenState();
}

class _ConfirmScreenState extends State<ConfirmScreen> {
  TextEditingController codeController = TextEditingController();
  String? errorText;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        elevation: 1,
        centerTitle: true,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back_ios),
        ),
        title: const ScreenTitle(title: "Подтверждение номера"),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 40),
        child: Column(children: [
          SizedBox(height: 25.h),
          const Text(
            "Введите код из смс",
            style: AppFonts.s22W500,
          ),
          SizedBox(height: 143.h),
          CodeTextField(
            onChanged: (val) {
              if (val == widget.code.toString()) {
                errorText = null;
                setState(() {});
              } else {
                errorText = "Код не совпадает";
                setState(() {});
              }
            },
            codeController: codeController,
            hintText: "Код из СМС",
            errorText: errorText,
          ),
          SizedBox(height: 24.h),
          TextButton(
            onPressed: () {
              widget.code = Random().nextInt(8999) + 1000;
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text(widget.code.toString()),
              ));
            },
            child: Text(
              "Получить код повторно",
              style: AppFonts.s15W400.copyWith(color: const Color(0xff007AFF)),
            ),
          ),
          // SizedBox(height: 50.h),
          const Spacer(),
          AppButton(
            onPressed: codeController.text.length < 4
                ? null
                : () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const CreateProfile()));
                  },
            name: "Далее",
          ),
          SizedBox(height: 25.h),
        ]),
      ),
    );
  }
}
