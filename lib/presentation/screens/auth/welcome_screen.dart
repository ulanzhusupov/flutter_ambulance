import 'package:flutter/material.dart';
import 'package:flutter_ambulance/data/app_data.dart';
import 'package:flutter_ambulance/presentation/screens/auth/login_screen.dart';
import 'package:flutter_ambulance/presentation/theme/app_fonts.dart';
import 'package:flutter_ambulance/presentation/widgets/app_button.dart';
import 'package:flutter_ambulance/presentation/widgets/shared_pref_widget.dart';
import 'package:flutter_ambulance/presentation/widgets/welcome_info.dart';
import 'package:flutter_ambulance/resources/resources.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        centerTitle: true,
        title: const Text(
          "Профиль",
          style: AppFonts.s17W600,
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.settings),
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 18.h,
            ),
            const Text(
              "Зачем нужен профиль?",
              style: AppFonts.s22W500,
            ),
            SizedBox(height: 10.h),
            Column(
              children: AppData.data
                  .map((e) => WelcomeInfoItem(image: e.image, text: e.title))
                  .toList(),
            ),
            AppButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const LoginScreen()));
                },
                name: "Войти"),
          ],
        ),
      ),
    );
  }
}
