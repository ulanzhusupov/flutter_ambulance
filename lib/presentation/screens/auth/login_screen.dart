import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_ambulance/core/app_consts.dart';
import 'package:flutter_ambulance/presentation/screens/auth/confirm_screen.dart';
import 'package:flutter_ambulance/presentation/theme/app_colors.dart';
import 'package:flutter_ambulance/presentation/theme/app_fonts.dart';
import 'package:flutter_ambulance/presentation/widgets/app_button.dart';
import 'package:flutter_ambulance/presentation/widgets/custom_textfield.dart';
import 'package:flutter_ambulance/presentation/widgets/screen_title.dart';
import 'package:flutter_ambulance/presentation/widgets/shared_pref_widget.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController phoneController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.close),
        ),
        title: const ScreenTitle(title: "Вход"),
        centerTitle: true,
        elevation: 1,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 15.h),
            const Text(
              "Войти",
              style: AppFonts.s34W700,
            ),
            SizedBox(height: 49.h),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 11),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Номер телефона",
                    style: AppFonts.s15W400,
                  ),
                  CustomTextField(
                    controller: phoneController,
                    hintText: "555 00 00 00",
                    prefixText: "0 ",
                    maxLength: 9,
                    onChanged: (val) {
                      setState(() {});
                    },
                  ),
                  SizedBox(height: 15.h),
                  const Text(
                    "На указанный вами номер придет однократное СМС-сообщение с кодом подтверждения.",
                    style: AppFonts.s15W400,
                  ),
                ],
              ),
            ),
            SizedBox(height: 50.h),
            AppButton(
                onPressed: phoneController.text.length < 9
                    ? null
                    : () async {
                        await SharedPrefWidget.prefs.setString(
                            AppConsts.phoneNumber, phoneController.text);
                        int randCode = Random().nextInt(8999) + 1000;
                        ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: Text(randCode.toString())));
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ConfirmScreen(
                                      code: randCode,
                                    )));
                      },
                name: "Далее")
          ],
        ),
      ),
    );
  }
}
