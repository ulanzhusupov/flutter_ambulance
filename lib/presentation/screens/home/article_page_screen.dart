import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ambulance/data/models/article_model.dart';
import 'package:flutter_ambulance/presentation/theme/app_colors.dart';
import 'package:flutter_ambulance/presentation/theme/app_fonts.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

@RoutePage()
class ArticlePageScreen extends StatelessWidget {
  const ArticlePageScreen({super.key, required this.model});

  final ArticleModel model;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          "Статья",
          style: AppFonts.s17W600,
        ),
        shape: const Border(
            bottom: BorderSide(
          color: AppColors.appBarBorderColor,
          width: 0.5,
        )),
      ),
      body: Scrollbar(
        thickness: 8,
        interactive: true,
        radius: const Radius.circular(5), //corner radius of scrollbar
        scrollbarOrientation: ScrollbarOrientation.right,
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 23.h, horizontal: 16.w),
            child: Column(
              children: [
                Text(
                  model.title,
                  style: AppFonts.s24W500,
                ),
                SizedBox(height: 15.h),
                Text(
                  model.annotation,
                  style: AppFonts.annotationStyle,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 12),
                  child: Row(
                    children: [
                      CircleAvatar(
                          backgroundImage: AssetImage(model.authorPhoto)),
                      SizedBox(width: 8.w),
                      Text(
                        model.author,
                        style: AppFonts.s13W400,
                      ),
                      SizedBox(width: 8.w),
                      Text(
                        "${model.date.substring(0, 6)}. · Время чтения: ${model.readTime} мин",
                        style: AppFonts.s13W400
                            .copyWith(color: AppColors.greyText),
                        textScaleFactor: 1,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: double.infinity,
                  height: 182.h,
                  child: Image.asset(
                    model.cover,
                    fit: BoxFit.cover,
                    alignment: Alignment.center,
                  ),
                ),
                SizedBox(height: 43.h),
                Text(
                  model.text,
                  style: AppFonts.s18W400,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  String formatDate(String date) {
    return "${date.substring(0, 7)}.";
  }
}
