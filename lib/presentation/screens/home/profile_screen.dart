import 'dart:io';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ambulance/core/app_consts.dart';
import 'package:flutter_ambulance/presentation/theme/app_colors.dart';
import 'package:flutter_ambulance/presentation/theme/app_fonts.dart';
import 'package:flutter_ambulance/presentation/widgets/shared_pref_widget.dart';
import 'package:flutter_ambulance/presentation/widgets/tabbar/custom_tabbar.dart';
import 'package:flutter_ambulance/presentation/widgets/tabbar/profile_tabbar_view.dart';
import 'package:flutter_ambulance/resources/resources.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';

@RoutePage()
class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  String? imagePath;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Мой профиль",
          style: AppFonts.s22W500,
        ),
        actions: [
          IconButton(onPressed: () {}, icon: const Icon(Icons.settings))
        ],
      ),
      body: Center(
        child: Column(
          children: [
            SizedBox(height: 16.h),
            Stack(children: [
              CircleAvatar(
                radius: 50.r,
                backgroundImage:
                    imagePath != null ? FileImage(File(imagePath!)) : null,
                backgroundColor: AppColors.lightBlue,
                child: imagePath != null
                    ? null
                    : Text(
                        '${SharedPrefWidget.prefs.getString(AppConsts.name)?[0] ?? ''} ${SharedPrefWidget.prefs.getString(AppConsts.surname)?[0] ?? ''}',
                        style:
                            AppFonts.s34W700.copyWith(color: AppColors.white),
                      ),
              ),
              Positioned(
                bottom: 0,
                right: 0,
                child: CircleAvatar(
                  radius: 16.r,
                  backgroundColor: AppColors.buttonColor,
                  child: InkWell(
                    onTap: () {
                      openDialog();
                    },
                    child: const Icon(
                      Icons.camera_alt_outlined,
                      color: AppColors.white,
                    ),
                  ),
                ),
              )
            ]),
            SizedBox(height: 16.h),
            Text(
              '${SharedPrefWidget.prefs.getString(AppConsts.name)} ${SharedPrefWidget.prefs.getString(AppConsts.surname)}',
              style: AppFonts.s22W500,
            ),
            Text(
              '+996${SharedPrefWidget.prefs.getString(AppConsts.phoneNumber)}',
              style: AppFonts.s18W400,
            ),
            SizedBox(height: 13.h),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.49,
              child: const DefaultTabController(
                length: 3,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    children: [
                      CustomTabbar(
                        tab1: "Анализы",
                        tab2: "Диагнозы",
                        tab3: "Рекомендации",
                        tab1IconStr: SvgAssets.eyedropperSvg,
                        tab2IconStr: SvgAssets.collectionSvg,
                        tab3IconStr: SvgAssets.recomendSvg,
                      ),
                      Expanded(
                        child: ProfileTabbarView(),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  String formatNumber() {
    String result = "";
    return result;
  }

  void openDialog() {
    showDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        content: Column(
          children: [
            const Text(
              "Загрузить фото через:",
              style: AppFonts.s17W600,
              textAlign: TextAlign.center,
            ),
            const Divider(),
            TextButton(
              onPressed: () {
                pickProfileImage(ImageSource.camera);
                Navigator.pop(context);
              },
              child: const Text(
                'Camera',
                style: AppFonts.s22W500,
              ),
            ),
            TextButton(
              onPressed: () {
                pickProfileImage(ImageSource.gallery);
                Navigator.pop(context);
              },
              child: const Text(
                'Galery',
                style: AppFonts.s22W500,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> pickProfileImage(ImageSource imageSource) async {
    final ImagePicker imagePicker = ImagePicker();

    try {
      XFile? file = await imagePicker.pickImage(source: imageSource);
      if (file != null) {
        imagePath = file.path;
        setState(() {});
      }
    } catch (e) {
      print(e.toString());
    }
  }
}
