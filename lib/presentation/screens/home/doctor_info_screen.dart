import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ambulance/data/models/doctors_model.dart';
import 'package:flutter_ambulance/presentation/theme/app_colors.dart';
import 'package:flutter_ambulance/presentation/theme/app_fonts.dart';
import 'package:flutter_ambulance/presentation/widgets/app_button.dart';
import 'package:flutter_ambulance/presentation/widgets/doctor_contacts.dart';
import 'package:flutter_ambulance/presentation/widgets/doctors_feedback.dart';
import 'package:flutter_ambulance/presentation/widgets/tabbar/custom_tabbar.dart';
import 'package:flutter_ambulance/resources/resources.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

@RoutePage()
class DoctorInfoScreen extends StatefulWidget {
  const DoctorInfoScreen({super.key, required this.model});
  final DoctorsModel model;

  @override
  State<DoctorInfoScreen> createState() => _DoctorInfoScreenState();
}

class _DoctorInfoScreenState extends State<DoctorInfoScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          widget.model.name,
          style: AppFonts.s17W600,
        ),
        actions: [
          IconButton(
              onPressed: () {},
              icon: const Icon(Icons.bookmark_border_outlined))
        ],
        shape: const Border(
            bottom: BorderSide(color: AppColors.appBarBorderColor, width: 0.3)),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: 30.h),
              CircleAvatar(
                backgroundImage: NetworkImage(widget.model.image),
                radius: 50.r,
              ),
              SizedBox(height: 17.h),
              Text(
                widget.model.name,
                style: AppFonts.s22W500,
              ),
              SizedBox(height: 8.h),
              Text(
                widget.model.speciality,
                style: AppFonts.s18W400,
              ),
              SizedBox(height: 20.h),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RatingBar.builder(
                    glow: true,
                    glowRadius: 5,
                    itemSize: 24.w,
                    initialRating: widget.model.rating,
                    minRating: 0,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    itemPadding: const EdgeInsets.symmetric(horizontal: 4.0),
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: AppColors.ratingColor,
                    ),
                    unratedColor: AppColors.ratingBackgroundColor,
                    onRatingUpdate: (rating) {
                      print(rating);
                    },
                  ),
                  SizedBox(width: 10.w),
                  Text(
                    widget.model.rating.toString(),
                    style: AppFonts.s18W400
                        .copyWith(color: AppColors.ratingBackgroundColor),
                  ),
                ],
              ),
              DefaultTabController(
                length: 3,
                child: SizedBox(
                  height: MediaQuery.of(context).size.height * 0.6,
                  child: Column(
                    children: [
                      const CustomTabbar(
                        tab1: "О докторе",
                        tab2: "Контакты",
                        tab3: "Отзывы",
                        tab1IconStr: SvgAssets.userPinSvg,
                        tab2IconStr: SvgAssets.locationSvg,
                        tab3IconStr: SvgAssets.messageDoctorSvg,
                      ),
                      Expanded(
                        child: TabBarView(
                          physics: const BouncingScrollPhysics(),
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: ListView(
                                children: [
                                  Center(
                                    child: Text(
                                      widget.model.info,
                                      style: AppFonts.s18W400,
                                    ),
                                  ),
                                  const Spacer(),
                                  SizedBox(height: 15.h),
                                  AppButton(
                                      onPressed: () {},
                                      name: "Записаться на приём"),
                                ],
                              ),
                            ),
                            DoctorContacts(
                              phoneNumber: widget.model.phoneNumber,
                              address: widget.model.address,
                              email: widget.model.email,
                            ),
                            DoctorsFeedback(
                              itemCount: widget.model.feedback.length,
                              model: widget.model.feedback,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
