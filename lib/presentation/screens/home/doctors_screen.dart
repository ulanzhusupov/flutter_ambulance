import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ambulance/data/models/doctors_model.dart';
import 'package:flutter_ambulance/presentation/screens/home/doctor_info_screen.dart';
import 'package:flutter_ambulance/presentation/screens/splash_screen.dart';
import 'package:flutter_ambulance/presentation/theme/app_colors.dart';
import 'package:flutter_ambulance/presentation/theme/app_fonts.dart';
import 'package:flutter_ambulance/presentation/widgets/doctors_tile.dart';
import 'package:flutter_ambulance/presentation/widgets/my_chip.dart';
import 'package:flutter_ambulance/presentation/widgets/search_textfield.dart';
import 'package:flutter_ambulance/resources/resources.dart';
import 'package:flutter_ambulance/routes/app_router.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

@RoutePage()
class DoctorsScreen extends StatefulWidget {
  const DoctorsScreen({
    super.key,
  });

  @override
  State<DoctorsScreen> createState() => _DoctorsScreenState();
}

class _DoctorsScreenState extends State<DoctorsScreen> {
  List<String> speciality = ["Артимологи", "Кардиологи", "Кардиохирурги"];
  int chipCurrentIndex = 0;
  TextEditingController searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        // backgroundColor: AppColors.backgroundScreenGrey,
        body: Padding(
          padding: const EdgeInsets.all(16),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "Доктора",
                      style: AppFonts.s34W700,
                    ),
                    SvgPicture.asset(SvgAssets.bell)
                  ],
                ),
                SizedBox(height: 16.h),
                Row(
                  children: [
                    SearchTextField(
                      controller: searchController,
                      onChanged: null,
                      hintText: "Поиск врача",
                    ),
                    SizedBox(width: 10.w),
                    InkWell(
                      onTap: () {
                        searchController.clear();
                      },
                      child: const Text(
                        "Очистить",
                        style: AppFonts.s14W600,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.1.h,
                  child: ListView.builder(
                    itemBuilder: (context, index) => MyChip(
                      title: speciality[index],
                      onSelected: (val) {
                        chipCurrentIndex = index;
                        setState(() {});
                      },
                      isSelected: chipCurrentIndex == index,
                    ),
                    itemCount: speciality.length,
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                  ),
                ),
                Row(
                  children: [
                    Image.asset(Images.upAndDown, width: 19.w),
                    const Text(
                      "Сортировка:",
                      style: AppFonts.s14W600,
                    ),
                    SizedBox(width: 6.w),
                    InkWell(
                      child: Text(
                        "имя А-Я",
                        style: AppFonts.s14W600
                            .copyWith(color: AppColors.textBlue),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 12.h),
                Container(
                  height: MediaQuery.of(context).size.height * 0.64.h,
                  color: AppColors.backgroundScreenGrey,
                  child: ListView.builder(
                      itemCount: DoctorsData.doctors.length,
                      itemBuilder: (context, index) {
                        return DoctorsTile(
                          model: DoctorsData.doctors[index],
                          onTap: () {
                            context.router.push(DoctorInfoRoute(
                                model: DoctorsData.doctors[index]));
                            // Navigator.push(
                            //   context,
                            //   MaterialPageRoute(
                            //     builder: (context) => DoctorInfoScreen(
                            //         model: DoctorsData.doctors[index]),
                            //   ),
                            // );
                          },
                        );
                      }),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
