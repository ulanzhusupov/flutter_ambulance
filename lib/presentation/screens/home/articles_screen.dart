import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ambulance/data/models/article_model.dart';
import 'package:flutter_ambulance/presentation/screens/home/article_page_screen.dart';
import 'package:flutter_ambulance/presentation/theme/app_colors.dart';
import 'package:flutter_ambulance/presentation/theme/app_fonts.dart';
import 'package:flutter_ambulance/presentation/widgets/article_tile.dart';
import 'package:flutter_ambulance/resources/resources.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

@RoutePage()
class ArticlesScreen extends StatelessWidget {
  const ArticlesScreen({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundScreenGrey,
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 16.h, horizontal: 20.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                "Статьи",
                style: AppFonts.s34W700,
              ),
              SizedBox(height: 16.h),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.73.h,
                child: Scrollbar(
                  thickness: 8,
                  interactive: true,
                  radius: const Radius.circular(5), //corner radius of scrollbar
                  scrollbarOrientation: ScrollbarOrientation.right,
                  child: ListView.builder(
                    itemCount: ArticleData.articles.length,
                    itemBuilder: (context, index) => ArticleTile(
                      articleModel: ArticleData.articles[index],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
