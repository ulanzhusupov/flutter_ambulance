import 'package:flutter/material.dart';
import 'package:flutter_ambulance/presentation/theme/app_colors.dart';

abstract class AppFonts {
  static const TextStyle s17W600 = TextStyle(
      fontSize: 17, fontWeight: FontWeight.w600, color: AppColors.black);
  static const TextStyle s17W500 = TextStyle(
      fontSize: 17, fontWeight: FontWeight.w500, color: AppColors.black);
  static const TextStyle s17W400 = TextStyle(
      fontSize: 17, fontWeight: FontWeight.w400, color: AppColors.black);
  static const TextStyle s18W600 = TextStyle(
      fontSize: 18, fontWeight: FontWeight.w600, color: AppColors.black);
  static const TextStyle s18W400 = TextStyle(
      fontSize: 18, fontWeight: FontWeight.w400, color: AppColors.black);
  static const TextStyle s22W500 = TextStyle(
      fontSize: 22, fontWeight: FontWeight.w500, color: AppColors.black);
  static const TextStyle s15W400 = TextStyle(
      fontSize: 15, fontWeight: FontWeight.w400, color: AppColors.black);
  static const TextStyle s15W300 = TextStyle(
      fontSize: 15, fontWeight: FontWeight.w300, color: AppColors.black);
  static const TextStyle s15W500 = TextStyle(
      fontSize: 15, fontWeight: FontWeight.w500, color: AppColors.black);
  static const TextStyle s14W600 = TextStyle(
      fontSize: 14, fontWeight: FontWeight.w600, color: AppColors.black);
  static const TextStyle s13W400 = TextStyle(
      fontSize: 14, fontWeight: FontWeight.w600, color: AppColors.black);
  static const TextStyle s10W500 = TextStyle(
      fontSize: 10, fontWeight: FontWeight.w500, color: AppColors.black);
  static const TextStyle s24W500 = TextStyle(
      fontSize: 24, fontWeight: FontWeight.w500, color: AppColors.black);
  static const TextStyle s34W700 = TextStyle(
      fontSize: 34, fontWeight: FontWeight.w700, color: AppColors.black);
  static const TextStyle annotationStyle = TextStyle(
      fontSize: 18, fontWeight: FontWeight.w500, color: AppColors.greyText);
}
