import 'package:flutter/material.dart';

abstract class AppColors {
  static const Color black = Colors.black;
  static const Color white = Colors.white;
  static const Color buttonColor = Color(0xff4A86CC);
  static const Color grey = Color(0xff979797);
  static const Color lightGrey = Color(0xffAFAFAF);
  static const Color borderColor = Color(0xff333333);
  static const Color appBarBorderColor = Color.fromRGBO(0, 0, 0, 0.3);
  static const Color lightBlue = Color(0xffB6D8FF);
  static const Color greyText = Color(0xff757575);
  static Color searchColorOpacity = const Color(0xff8E8E93).withOpacity(0.12);
  static Color searchHintColor = const Color(0xff8E8E93);
  static Color textBlue = const Color(0xff2376F6);
  static Color backgroundScreenGrey = const Color(0xffEFEFF3);
  static Color ratingColor = const Color(0xffFC9E4F);
  static Color ratingBackgroundColor = const Color(0xffAFAFAF);
  static Color doctorDividerColor = const Color(0xffDFDFDF);
}
