part of 'resources.dart';

class SvgAssets {
  SvgAssets._();

  static const String addDoc = 'assets/svg/add_doc.svg';
  static const String ambulanceSvg = 'assets/svg/ambulance.svg.svg';
  static const String bell = 'assets/svg/bell.svg';
  static const String bookmarkSvg = 'assets/svg/bookmark.svg.svg';
  static const String collectionSvg = 'assets/svg/collection.svg.svg';
  static const String doctorCommentsSvg = 'assets/svg/doctor_comments.svg.svg';
  static const String eyedropperSvg = 'assets/svg/eyedropper.svg.svg';
  static const String locationSvg = 'assets/svg/location.svg.svg';
  static const String messageDoctorSvg = 'assets/svg/message-doctor.svg.svg';
  static const String recomendSvg = 'assets/svg/recomend.svg.svg';
  static const String spreadsheetSvg = 'assets/svg/spreadsheet.svg.svg';
  static const String star = 'assets/svg/star.svg';
  static const String userPinSvg = 'assets/svg/user-pin.svg.svg';
  static const String userPlusSvg = 'assets/svg/user-plus.svg.svg';
  static const String userSvg = 'assets/svg/user.svg.svg';
}
