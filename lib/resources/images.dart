part of 'resources.dart';

class Images {
  Images._();

  static const String articleAvatar = 'assets/png/article-avatar.png';
  static const String articleCover = 'assets/png/article-cover.jpg';
  static const String authorPhoto = 'assets/png/author-photo.png';
  static const String avatar1 = 'assets/png/avatar1.png';
  static const String bell = 'assets/png/bell.png';
  static const String bxSpreadsheetSvg = 'assets/png/bx-spreadsheet.svg.png';
  static const String clipboard = 'assets/png/clipboard.png';
  static const String clipboardMax = 'assets/png/clipboardMax.png';
  static const String fileFolder = 'assets/png/fileFolder.png';
  static const String hospital = 'assets/png/hospital.png';
  static const String pageWithCurl = 'assets/png/pageWithCurl.png';
  static const String sleepCover = 'assets/png/sleep-cover.jpg';
  static const String speech = 'assets/png/speech.png';
  static const String upAndDown = 'assets/png/up_and_down.png';
  static const String userPlus = 'assets/png/user-plus.png';
}
