part of 'resources.dart';

class Svg {
  Svg._();

  static const String ambulanceSvg = 'assets/svg/ambulance.svg.svg';
  static const String bookmarkSvg = 'assets/svg/bookmark.svg.svg';
  static const String spreadsheetSvg = 'assets/svg/spreadsheet.svg.svg';
  static const String userPlusSvg = 'assets/svg/user-plus.svg.svg';
  static const String userSvg = 'assets/svg/user.svg.svg';
}
