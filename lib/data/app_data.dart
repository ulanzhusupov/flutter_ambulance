import 'package:flutter_ambulance/data/models/info_widget_model.dart';
import 'package:flutter_ambulance/resources/resources.dart';

abstract class AppData {
  static List<InfoWidgetModel> data = [
    InfoWidgetModel(
        image: Images.hospital,
        title: "Записывайтесь на прием к самым лучшим специалистам"),
    InfoWidgetModel(
        image: Images.clipboard,
        title:
            "Сохраняйте результаты ваших анализов, диагнозы и рекомендации от врачей в собственную библиотеку"),
    InfoWidgetModel(
        image: Images.speech,
        title:
            "Просматривайте отзывы о врачах и дополняйте собственными комментариями"),
    InfoWidgetModel(
        image: Images.bell,
        title: "Получайте уведомления о приеме или о поступивших сообщениях"),
  ];
}
